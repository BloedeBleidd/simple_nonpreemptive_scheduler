/*
 * bloedebleidd_libraries.h
 *
 *  Created on: 28.06.2017
 *      Author: BloedeBleidd
 */

#ifndef BLOEDEBLEIDD_LIBRARIES_BLOEDEBLEIDD_LIBRARIES_H_
#define BLOEDEBLEIDD_LIBRARIES_BLOEDEBLEIDD_LIBRARIES_H_

/* please use here adequate define
 *
 * STM32 Ultra-low power series		#define STM32L0		#define STM32L1		#define STM32L4
 * STM32 Mainstream series			#define STM32F0		#define STM32F1		#define STM32F3
 * STM32 High performance series	#define STM32F2		#define STM32F4		#define STM32F7		#define STM32H7
*/
#define STM32F4

/* please set here which library you want to use (1+ = USE, 0 = NOT) */
#define USE_GPIO	1
#define USE_EXTI	0
#define USE_DMA		0
#define USE_ADC		0
#define USE_DAC		0
#define USE_TIMERS	0
#define USE_SPI		0
#define USE_I2C		0
#define USE_USART	0
#define USE_RTC		0





#if   defined (STM32L0)
	#include "stm32l0xx.h"
#elif defined (STM32L1)
	#include "stm32l1xx.h"
#elif defined (STM32L4)
	#include "stm32l4xx.h"
#elif defined (STM32F0)
	#include "stm32f0xx.h"
#elif defined (STM32F1)
	#include "stm32f10x.h"
#elif defined (STM32F2)
	#include "stm32f2xx.h"
#elif defined (STM32F3)
	#include "stm32f3xx.h"
#elif defined (STM32F4)
	#include "stm32f4xx.h"
#elif defined (STM32F7)
	#include "stm32f7xx.h"
#elif defined (STM32H7)
	#include "stm32h7xx.h"
#else
	#error please define uC series in BloedeBleidd_Libraries/bloedebleidd_libraries.h file!
#endif

#if USE_GPIO > 0
#include "GPIO/gpio.h"
#endif
#if USE_EXTI > 0
#include "EXTI/exti.h"
#endif
#if USE_DMA > 0
#include "DMA/dma.h"
#endif
#if USE_ADC > 0
#include "ADC/adc.h"
#endif
#if USE_DAC > 0
#include "DAC/dac.h"
#endif
#if USE_TIMERS > 0
#include "TIMERS/timers.h"
#endif
#if USE_SPI > 0
#include "SPI/spi.h"
#endif
#if USE_I2C > 0
#include "I2C/i2c.h"
#endif
#if USE_USART > 0
#include "USART/usart.h"
#endif
#if USE_RTC > 0
#include "RTC/rtc.h"
#endif

#endif /* BLOEDEBLEIDD_LIBRARIES_BLOEDEBLEIDD_LIBRARIES_H_ */
