/*
 * taskmanager.c
 *
 *  Created on: Nov 3, 2016
 *      Author: BloedeBleidd
 */

#include "taskmanager.h"
#include "stdlib.h"


/**************************************** Scheduler ****************************************/

typedef struct TaskData
{
	struct TaskData *nextTask;

	TaskPointer task;
	TaskPriority priority;
	volatile TaskStatus status;
	volatile TaskTime delay;
	TaskTime period;
} TaskData;


SchedulerResult schedulerClockInitialize( const uint16_t sample );
SchedulerResult schedulerInitialize( const uint16_t sample );
void schedulerIdleTask (void);


volatile SchedulerTick systemTickCnt = 0;
TaskData *idleTaskHook;


SchedulerResult schedulerInitialize( const uint16_t sample )
{
	if( schedulerClockInitialize(sample) )
	{
		return CLOCK_ERROR;
	}

	idleTaskHook = ( TaskData* )malloc( sizeof( TaskData ) );

	if( NULL == idleTaskHook )
	{
		return CREATE_ERROR;
	}

	idleTaskHook->task = schedulerIdleTask;
	idleTaskHook->priority = 0;
	idleTaskHook->status = TASK_AWAIT;
	idleTaskHook->delay = 1000;
	idleTaskHook->period = 1000;
	idleTaskHook->nextTask = NULL;

	return ALL_OK;
}

SchedulerResult schedulerCreateTask( const TaskPointer task, const TaskTime period, const TaskPriority priority)
{
	if( NULL == task )
	{
		return CREATE_ERROR;
	}

	TaskData *newTaskData = ( TaskData* )malloc( sizeof( TaskData ) );

	if( NULL == newTaskData )
	{
		return CREATE_ERROR;
	}

	newTaskData->task = task;
	newTaskData->priority = priority;
	newTaskData->status = TASK_AWAIT;
	newTaskData->delay = period;
	newTaskData->period	= period;
	newTaskData->nextTask = NULL;

	TaskData *tmp = idleTaskHook;

	while( NULL != tmp->nextTask)
	{
		tmp = tmp->nextTask;
	}

	tmp->nextTask = newTaskData;

	return ALL_OK;
}

SchedulerResult schedulerSearchTask( const TaskPointer task, TaskData *taskList)
{
	if( NULL == task )
	{
		return TASK_NOT_EXIST;
	}

	TaskData *tmp = idleTaskHook;

	while( task != tmp->task)
	{
		if( NULL == tmp->nextTask)
		{
			return TASK_NOT_EXIST;
		}

		tmp = tmp->nextTask;
	}

	taskList = tmp;

	return ALL_OK;
}

SchedulerResult schedulerEditTaskStatus( const TaskPointer task, const TaskStatus status)
{
	TaskData *tmp = NULL;

	if( ALL_OK != schedulerSearchTask( task, tmp) )
	{
		return TASK_NOT_EXIST;
	}

	tmp->status = status;

	return ALL_OK;
}

TaskStatus schedulerGetTaskStatus ( const TaskPointer task )
{
	TaskData *tmp = NULL;

	if( ALL_OK != schedulerSearchTask( task, tmp) )
	{
		return SOMETHING_GONE_WRONG;
	}

	return tmp->status;
}

SchedulerResult schedulerEditTaskPeriod( const TaskPointer task, const TaskTime period)
{
	TaskData *tmp = NULL;

	if( ALL_OK != schedulerSearchTask( task, tmp) )
	{
		return TASK_NOT_EXIST;
	}

	tmp->period = period;

	return ALL_OK;
}

TaskTime schedulerGetTaskPeriod ( const TaskPointer task )
{
	TaskData *tmp = NULL;

	if( ALL_OK != schedulerSearchTask( task, tmp) )
	{
		return SOMETHING_GONE_WRONG;
	}

	return tmp->period;
}

SchedulerResult schedulerEditTaskDelay( const TaskPointer task, const TaskTime delay)
{
	TaskData *tmp = NULL;

	if( ALL_OK != schedulerSearchTask( task, tmp) )
	{
		return TASK_NOT_EXIST;
	}

	tmp->delay = delay;

	return ALL_OK;
}

TaskTime schedulerGetTaskDelay ( const TaskPointer task )
{
	TaskData *tmp = NULL;

	if( ALL_OK != schedulerSearchTask( task, tmp) )
	{
		return SOMETHING_GONE_WRONG;
	}

	return tmp->delay;
}

SchedulerTick schedulerGetTickCounter( void )
{
	return systemTickCnt;
}

void schedulerStart( void )
{
	TaskPriority highestPriority;
	TaskData *taskToExecute;
	TaskData *tmp;

	while(1)
	{
		highestPriority = 0;
		taskToExecute = NULL;
		tmp = idleTaskHook;

		do
		{
			if( TASK_READY_TO_HANDLE == tmp->status && highestPriority <= tmp->priority)
			{
				highestPriority = tmp->priority;
				taskToExecute = tmp;
			}

			tmp = tmp->nextTask;
		} while( NULL != tmp);

		if( NULL != taskToExecute )
		{
			taskToExecute->delay = taskToExecute->period;
			taskToExecute->status = TASK_AWAIT;
			taskToExecute->task();
		}
	}
}

void schedulerIdleTask (void)
{

}

/**************************************** Timers ****************************************/

SchedulerResult schedulerClockInitialize( const uint16_t sample )
{
	if (SysTick_Config(SystemCoreClock / sample))
	{
		return CLOCK_ERROR;
	}

	return ALL_OK;
}

void SysTick_Handler(void)
{
	systemTickCnt++;

	TaskData *tmp = idleTaskHook;

	do
	{
		if( TASK_AWAIT == tmp->status)
		{
			if(tmp->delay)
			{
				tmp->delay--;
			}
			else
			{
				tmp->status = TASK_READY_TO_HANDLE;
			}
		}

		tmp = tmp->nextTask;
	} while( NULL != tmp);
}
