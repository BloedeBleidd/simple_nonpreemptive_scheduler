/*
 * taskmanager.h
 *
 *  Created on: Nov 3, 2016
 *      Author: BloedeBleidd
 */

#ifdef __cplusplus
extern "C" {
#endif


#ifndef TASKMANAGER_H_
#define TASKMANAGER_H_


#include "stm32f4xx.h"

/**************************************** Configuration ****************************************/

#define USE_CPU_USAGE 		0

/**************************************** Timers ****************************************/

#if USE_CPU_USAGE == 1
#define CPU_USAGE_TIMER		TIM4
#define CPU_TIMER_ENABLE	RCC->APB1ENR |= RCC_APB1ENR_TIM4EN;
#endif


/**************************************** Scheduler ****************************************/

typedef void (*TaskPointer)(void);
typedef uint8_t TaskPriority;
typedef uint32_t TaskTime;
typedef uint32_t SchedulerTick;

typedef enum
{
	SOMETHING_GONE_WRONG,
	TASK_BLOCKED,
	TASK_AWAIT,
	TASK_READY_TO_HANDLE,
} TaskStatus;

typedef enum
{
	ALL_OK,
	CLOCK_ERROR,
	CREATE_ERROR,
	TASK_NOT_EXIST
} SchedulerResult;

SchedulerResult schedulerInitialize( const uint16_t sample );

SchedulerResult schedulerCreateTask( const TaskPointer task, const TaskTime period, const TaskPriority priority);

SchedulerResult schedulerEditTaskStatus( TaskPointer task, const TaskStatus status);
TaskStatus schedulerGetTaskStatus ( const TaskPointer task );

SchedulerResult schedulerEditTaskPeriod( TaskPointer task, const TaskTime period);
TaskTime schedulerGetTaskPeriod ( const TaskPointer task );

SchedulerResult schedulerEditTaskDelay( TaskPointer task, const TaskTime delay);
TaskTime schedulerGetTaskDelay ( const TaskPointer task );

SchedulerTick schedulerGetTickCounter( void );

void schedulerStart( void );

#endif /* TASKMANAGER_H_ */

#ifdef __cplusplus
}
#endif
