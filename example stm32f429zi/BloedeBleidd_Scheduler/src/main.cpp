
/* Includes */
#include "stm32f4xx.h"
#include "main.h"
#include "BloedeBleidd_Libraries/bloedebleidd_libraries.h"
#include "TASK_MANAGER/taskmanager.h"

void testowyTask (void)
{
	volatile SchedulerTick sysTick;
	sysTick = schedulerGetTickCounter();

	sysTick += 1;
}

void jakiesCos (void)
{
	volatile SchedulerTick sysTick;
	sysTick = schedulerGetTickCounter();

	sysTick += 1;

	gpioBitToggle(LED_RED_GPIO, LED_RED_PIN);
}


int main(void)
{
	gpioInitialize();
	gpioPinConfiguration(LED_RED_GPIO, LED_RED_PIN, GPIO_MODE_OUTPUT_PUSH_PULL_LOW_SPEED);
	schedulerInitialize(1000);

	schedulerCreateTask(testowyTask, 1000, 1);
	schedulerCreateTask(jakiesCos, 500, 2);

	schedulerStart();

}
