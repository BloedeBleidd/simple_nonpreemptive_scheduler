/*
 * main.h
 *
 *  Created on: 04.07.2017
 *      Author: BloedeBleidd
 */

#ifndef MAIN_H_
#define MAIN_H_

#define LED_GREEN_GPIO	GPIOG
#define LED_GREEN_PIN	13

#define LED_RED_GPIO	GPIOG
#define LED_RED_PIN		14

#define LEDS_TASK_PRIORITY tskLOWEST_PRIORITY

#define KEY_GPIO		GPIOA
#define KEY_PIN			0



#endif /* MAIN_H_ */
